package net.tncy.vbe.validator;

import javax.validation.Constraint;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Constraint(validatedBy = ISBNValidator.class)
public @interface ISBN { }
