package net.tncy.vbe.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ISBNValidator implements ConstraintValidator<ISBN, String> {
    @Override
    public void initialize(ISBN isbn) {
    }

    @Override
    public boolean isValid(String bookNumber, ConstraintValidatorContext constraintContext) {
        return bookNumber != null && (bookNumber.length() == 13 || bookNumber.length() == 10);
    }
}
